export const MUTATION_UPDATE_USER = `
mutation updateUser($input: UpdateUserInput! ){
    success: updateUser(input: $input)
}
`;
