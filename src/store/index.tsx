import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';

import { creatorReducer } from './module/creator';

const rootReducer = combineReducers({
    creatorReducer: creatorReducer,
});

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
export type RootState = ReturnType<typeof rootReducer>;
export type Dispatch = typeof store.dispatch;
