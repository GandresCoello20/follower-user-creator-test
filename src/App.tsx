import React from 'react';

import { Provider } from 'react-redux';
import { store } from './store';

import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Home } from './components/pages/Home/home';

// import './styles/App.less';
import 'antd/dist/antd.css';

export function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}
