import React, { useState } from 'react';
import { Form, Input, Button, Row, Col, Card, Avatar, Select, notification } from 'antd';
import  { fetchGraphql } from '../../../graphql';
import { Creator } from '../../../graphql/creators/interfaces';
import { User } from '../../../graphql/users/interfaces';
import { MUtatioFollowAdminVariables, MutationFollwerAdmin, MUTATION_FOLLOW_ADMIN } from '../../../graphql/follower';
import { DEFAULT_AVATAR } from '../../../config';
import { GetCreator, GET_CREATOR_QUERY, GET_CREATOR_QUERY_USER, GetCreatorVariables, GetCreatorUserVariables } from '../../../graphql/creators';
import './home.less';
import { SelectValue } from 'antd/lib/select';

export function Home() {

    const { Meta } = Card;

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    };

    const [form] = Form.useForm();
    const [stateCreador, setStateCreador] = useState<Creator| null>(null);
    const [stateUser, setStateUser] = useState<User | null>(null);
    const [duration, setDuration] = useState<number>(0);

    const searchCreador = (event: React.ChangeEvent<HTMLInputElement>) => {
        const getCreator = async (username: string) => {
            const { creator } = await fetchGraphql<GetCreator, GetCreatorVariables>({
                query: GET_CREATOR_QUERY,
                variables: { username:  username}
            });

            setStateCreador(creator);
            console.log(creator);
        }

        getCreator(event.target.value);
    }

    const searchUser = (event: React.ChangeEvent<HTMLInputElement>) => {
        const getUser = async (email: string) => {
            const { creator } = await fetchGraphql<GetCreator, GetCreatorUserVariables>({
                query: GET_CREATOR_QUERY_USER,
                variables: { email: email }
            });

            setStateUser(creator);
        }

        getUser(event.target.value);
    }

    function selectTIme(value: SelectValue) {
        console.log(`selected ${value}`);
        setDuration(Number(value));
    }

    const onFinish = (values: any) => {
        const createFollow = async (user_id: any, creator_id: any, duration_time: number) => {
            const data = await fetchGraphql<MutationFollwerAdmin, MUtatioFollowAdminVariables>({
                query: MUTATION_FOLLOW_ADMIN,
                variables: { user_id, creator_id, method_subscription: 'paypal', id_subscription: 'idx', duration_time, },
            });

            if (data.success) {
                console.log('corrio perfecto');
                notification.open({
                    message: `Se creo el follower corectamente`,
                });
            } else {
                console.log('fallo');
                notification.open({
                    message: `hoo..!! algo salio mal`,
                });
            }
            return data.success;
        };
        createFollow(stateUser?._id, stateCreador?._id, duration);

        onReset();
    };

    const onReset = () => {
        form.resetFields();
    };

    return(
        <>
            <Row justify='center' className='row-home'>
                <Col xs={24}> <h2>Relacion creator - user</h2> </Col>
                <Col xs={20} md={16}>
                    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
                        <Form.Item name="creador" label="Creador" rules={[{ required: true }]}>
                            <Input placeholder="username" onChange={searchCreador} />
                        </Form.Item>

                        <Form.Item name="user" label="User" rules={[{ required: true }]}>
                            <Input placeholder="email" onChange={searchUser} />
                        </Form.Item>

                        <Form.Item name="tiempo" label="tiempo de duracion" rules={[{ required: true }]}>
                            <Select placeholder='selecionar tiempo' style={{ width: 120 }} onChange={selectTIme}>
                                <Select.Option value="seleccionar-tiempo" disabled>seleccionar tiempo</Select.Option>
                                <Select.Option value="1">1 mes</Select.Option>
                                <Select.Option value="2">2 meses</Select.Option>
                                <Select.Option value="3">3 meses</Select.Option>
                                <Select.Option value="0">indefinido</Select.Option>
                            </Select>
                        </Form.Item>
<br/>
                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                            &nbsp; &nbsp; &nbsp;
                            <Button htmlType="button" onClick={onReset}>
                                Reset
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>

            <Row justify='center' className='row-card'>
                <Col xs={20} md={10}>
                    <h2>Creador</h2>
                    <Card
                        style={{ width: 300 }}
                    >
                        <Meta
                            avatar={<Avatar src={stateCreador?.avatar === undefined ? DEFAULT_AVATAR : stateCreador?.avatar} />}
                            title={stateCreador?.username === undefined ? 'Sin resultados' : stateCreador?.username + ' ' + stateCreador?.email}
                            description={stateCreador?.about === undefined ? 'Sin resultados' : stateCreador?.about }
                        />
                    </Card>,
                </Col>
                <Col xs={20} md={10}>
                    <h2>Usuario</h2>
                    <Card
                        style={{ width: 300 }}
                    >
                        <Meta
                            avatar={<Avatar src={stateUser?.avatar === undefined ? DEFAULT_AVATAR : stateUser?.avatar} />}
                            title={stateUser?.email === undefined ? 'Sin resultados' : stateUser?.email}
                        />
                    </Card>,
                </Col>
                <Col xs={20}>
                    <p>Por defecto Grahpql obtiene el username <strong>jpmayorga</strong> cuando no se especifica el username.</p>
                </Col>
            </Row>
        </>
    );
}