import axios from 'axios';
import { GRAPHQL_URL } from '../config';

interface Headers {
    'Content-Type': string;
    Authorization?: string;
}

export async function fetchGraphql<QueryType, VariablesTypes>(options: {
    query: string;
    variables?: VariablesTypes;
}): Promise<QueryType> {
    let headers: Headers = {
        'Content-Type': 'application/json',
    };
    const request = await axios.post(
        GRAPHQL_URL,
        {
            query: options.query,
            variables: options.variables,
        },
        {
            headers,
        },
    );
    return await request.data.data;
}
