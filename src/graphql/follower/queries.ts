export const GET_MY_FOLLOWING = `
query GetMyFollowing {
    following: getFollowing {
        _id
        username
        avatar
        name
    }
}
`;

export const GET_FOLLOWING_QUERY = `
query GetFollowing($user_id: String!) {
    following: getFollowing(user_id: $user_id) {
        _id
        username
        avatar
        name
    }
}
`;

export const GET_TODAY_FOLLOWERS_COUNT_QUERY = `
query getTodayFollowersCount($creator_id: String ){
    count: getTodayFollowersCount(creator_id: $creator_id)
}
`;
